from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text

from models.account   import Account
from models.simulator import Simulator

class Experiment(Base):
  __tablename__ = 'experiments'

  id                        = Column(Integer, primary_key=True)
  name                      = Column(String(128))
  tags                      = Column(String(128))
  simulator_id              = Column(Integer, ForeignKey('simulators.id'))
  forked_from_id            = Column(Integer, ForeignKey('experiments.id'))
  account_id                = Column(Integer, ForeignKey('accounts.id'))
  configuration_document_id = Column(String(128))
  results_document_id       = Column(String(128))
