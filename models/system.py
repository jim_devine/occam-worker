from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Float

class System(Base):
  __tablename__ = 'systems'

  id              = Column(Integer, primary_key=True)
  simulators_path = Column(String)
  traces_path     = Column(String)
  jobs_path       = Column(String)
