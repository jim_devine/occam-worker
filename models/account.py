from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey

class Account(Base):
  __tablename__ = 'accounts'

  id       = Column(Integer, primary_key=True)
  username = Column(String(128))
