from lib.document_store import DocumentStore

class SimulatorInputSchema:
  def query():
    return DocumentStore.session().simulator_input_schemas
