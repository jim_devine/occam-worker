from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text

class Simulator(Base):
  __tablename__ = 'simulators'

  id                        = Column(Integer, primary_key=True)
  name                      = Column(String(128))
  tags                      = Column(String(128))
  local_path                = Column(String(128))
  binary_path               = Column(String(128))
  script_path               = Column(String(128))
  input_schema_document_id  = Column(String(128))
  output_schema_document_id = Column(String(128))
