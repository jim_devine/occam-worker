from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Float

from models.experiment import Experiment

class Job(Base):
  __tablename__ = 'jobs'

  id            = Column(Integer, primary_key=True)
  experiment_id = Column(Integer, ForeignKey('experiments.id'))
  simulator_id  = Column(Integer, ForeignKey('simulators.id'))
  status        = Column(String)
  kind          = Column(String)
  depends_on_id = Column(Integer, ForeignKey('jobs.id'))
  log_file      = Column(String)
  elapsed_time  = Column(Float)
