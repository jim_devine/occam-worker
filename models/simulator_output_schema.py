from lib.document_store import DocumentStore

class SimulatorOutputSchema:
  def query():
    return DocumentStore.session().simulator_output_schemas
