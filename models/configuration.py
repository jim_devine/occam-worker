from lib.document_store import DocumentStore

class Configuration:
  def query():
    return DocumentStore.session().configurations
