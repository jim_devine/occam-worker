import os
import sys

from lib.db           import DB
from models.simulator import Simulator
from models.job       import Job

class Builder:
  def __init__(self, options, workingPath):
    self.options     = options
    self.workingPath = workingPath

  def run(self, job):
    simulator = DB.session().query(Simulator).filter_by(id=job.simulator_id).first()
    print("Building %s in %s" % (simulator.name, simulator.local_path))

    path = simulator.local_path

    path = path + "/occam"

    os.chdir(self.workingPath)
    script = "python %s/spawn.py -d %s -r %s -j %s -w %s -c 'sh build.sh'" % (
        os.path.dirname(__file__), # Launch spawn.py from occam-worker source
        os.getpid(),               # -d {dispatcher process id}
        self.workingPath,          # -r {job working directory}
        job.id,                    # -j {job id}
        path)                      # -w {working directory for running job}

    # Do process spawning/monitoring in a separate space
    os.system(script)

    # Success
    return True

  def finish(self, job, workingPath):
    return
