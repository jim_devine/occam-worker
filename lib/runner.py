import os
import sys
import json

from lib.db               import DB
from models.experiment    import Experiment
from models.simulator     import Simulator
from models.configuration import Configuration
from models.result        import Result
from models.job           import Job

from bson.objectid        import ObjectId
from bson.json_util       import dumps

class Runner:
  """This class will handle running experiments and spawning experiment
  processes.
  """

  def __init__(self, options, workingPath):
    """Construct a runner class."""

    self.options     = options
    self.workingPath = workingPath

  def run(self, job):
    """Run the experiment process."""

    experiment = DB.session().query(Experiment).filter_by(id=job.experiment_id).first()
    simulator = DB.session().query(Simulator).filter_by(id=experiment.simulator_id).first()

    print("Running %s on simulator %s in %s" % (experiment.name, simulator.name, self.workingPath))

    path = simulator.local_path

    path = path + "/occam"

    os.chdir(self.workingPath)

    # Dump input.json
    print("Dumping simulator input json to %s/input.json" % (self.workingPath))
    input_file = open('input.json', 'w')
    print(experiment.configuration_document_id)
    configuration = Configuration.query().find_one({"_id": ObjectId(experiment.configuration_document_id)})
    print(configuration)
    input_file.write(dumps(configuration))
    input_file.close()

    script = "python %s/spawn.py -d %s -r %s -j %s -w %s -c 'python %s/launch.py'" % (
        os.path.dirname(__file__), # Launch spawn.py from occam-worker source
        os.getpid(),               # -d {dispatcher process id}
        self.workingPath,          # -r {job working directory}
        job.id,                    # -j {job id}
        self.workingPath,          # -w {working directory for running job}
        path)                      # python {occam script directory}/launch.py

    # Do process spawning/monitoring in a separate space
    os.system(script)

    # Success
    return True

  def finish(self, job, workingPath):
    """Clean up experiment results and store final results information."""

    output_path = "%s/output.json" % (workingPath)
    experiment = DB.session().query(Experiment).filter_by(id=job.experiment_id).first()

    if os.path.exists(output_path):
      print("Recording output.json to database")
      output_json = json.load(open(output_path))
      record = Result.query().insert(output_json)
      experiment.results_document_id = str(record)
    else:
      print("Output not found at %s/output.json" % (workingPath))
