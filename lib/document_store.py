from pymongo import MongoClient

class DocumentStore:
  engine = None
  db = None

  def initialize(options):
    if DocumentStore.db:
      DocumentStore.close()

    print("Initializing connection to document store")

    # Default host/port
    host = 'localhost'
    port = 27017
    name = 'occam'

    if options.mongo_host:
      host = options.mongo_host

    if options.mongo_port:
      port = options.mongo_port

    if options.mongo_name:
      name = options.mongo_name

    DocumentStore.engine = MongoClient(host, port)
    DocumentStore.db = DocumentStore.engine[name]

    return DocumentStore.db

  def close():
    DocumentStore.engine.close()
    DocumentStore.db = None

  def ordered_hash(value):
    for k, v in value.items():
      if isinstance(value[k], dict):
        value[k] = DocumentStore.ordered_hash(value[k])

    value["__ordering"] = list(value.keys())

    return value

  def session():
    return DocumentStore.db
