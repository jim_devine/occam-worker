from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy                 import create_engine
from sqlalchemy.orm             import sessionmaker

Base = declarative_base()

class DB:
  engine = None
  db = None

  def initialize(options):
    if DB.db:
      DB.close()

    print("Initializing connection to database")

    if options.sqlite:
      print("Opening sqlite database")
      DB.engine = create_engine('sqlite:///../occam-web/dev.db')
    else:
      print("Opening postgres database")
      DB.engine = create_engine('postgres://wilkie:@localhost/occam')

    Session   = sessionmaker(bind=DB.engine)

    DB.db = Session()

    print("Database connection established")
    return DB.db

  def close():
    DB.db.close()
    DB.db = None
    DB.engine = None

  def session():
    return DB.db
