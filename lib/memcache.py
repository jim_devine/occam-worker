import json

from pymemcache.client import Client

class Memcache:
  client = None

  def initialize(options):
    print("Initializing connection to memcache ephemeral storage")

    Memcache.client = Client(('localhost', 11211))
    return Memcache.client

  def get(key):
    Memcache.client.get(key)

  def set(key, value):
    Memcache.client.set(key, json.dumps(value))
