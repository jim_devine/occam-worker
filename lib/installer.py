import os          # path functions
import json        # For parsing recipes
import collections # For OrderedDict

from lib.db                         import DB
from lib.document_store             import DocumentStore
from models.simulator               import Simulator
from models.simulator_output_schema import SimulatorOutputSchema
from models.simulator_input_schema  import SimulatorInputSchema
from models.result                  import Result
from models.configuration           import Configuration
from models.recipe                  import Recipe
from models.job                     import Job

class Installer:
  def __init__(self, options, workingPath):
    self.options     = options
    self.workingPath = workingPath

  def run(self, job):
    simulator = DB.session().query(Simulator).filter_by(id=job.simulator_id).first()
    print("Installing %s in %s" % (simulator.name, simulator.local_path))

    path = simulator.local_path

    if simulator.binary_path.endswith(".tar") or simulator.binary_path.endswith(".gz") or simulator.binary_path.endswith(".bz") or simulator.binary_path.endswith(".tgz"):
      print("  from tar: %s" % (simulator.binary_path))

      if not os.path.exists(simulator.local_path):
        print("Making simulator directory: %s" % (simulator.local_path))
        os.mkdir(simulator.local_path)

      os.chdir(self.workingPath)
      script = "python %s/spawn.py -d %s -r %s -j %s -w %s -c 'git clone %s %s/occam' -c 'wget -Nc %s -O %s/source.tar' -c 'tar -xvf %s/source.tar -C %s'" % (
          os.path.dirname(__file__),  # Launch spawn.py from occam-worker source
          os.getpid(),                # -d {dispatcher process id}
          self.workingPath,           # -r {job working directory}
          job.id,                     # -j {job id}
          self.options.simulatorPath, # -w {working directory for running job}
          simulator.script_path,      # download occam scripts from
          simulator.local_path,       # put scripts here
          simulator.binary_path,      # download tar from
          simulator.local_path,       # download tar to
          simulator.local_path,       # tar file is here
          simulator.local_path)       # untar here

    else:
      print("  from git: %s" % (simulator.binary_path))

      os.chdir(self.workingPath)
      script = "python %s/spawn.py -d %s -r %s -j %s -w %s -c 'git clone %s %s' -c 'git clone %s %s/occam'" % (
          os.path.dirname(__file__),  # Launch spawn.py from occam-worker source
          os.getpid(),                # -d {dispatcher process id}
          self.workingPath,           # -r {job working directory}
          job.id,                     # -j {job id}
          self.options.simulatorPath, # -w {working directory for running job}
          simulator.binary_path,      # install simulator from
          simulator.local_path,       #    "        "     to
          simulator.script_path,      # install occam scripts from
          simulator.local_path)       #    "      "     "     to

    print("Running: %s" % (script))

    # Do process spawning/monitoring in a separate space
    os.system(script)

    # Success
    return True

  def finish(self, job, workingPath):
    simulator = DB.session().query(Simulator).filter_by(id=job.simulator_id).first()

    print("Finishing install task of %s in %s" % (simulator.name, simulator.local_path))

    # Look for input schema, etc
    input_schema_path = "%s/occam/input_schema.json" % (simulator.local_path)
    if os.path.exists(input_schema_path):
      print("Commiting simulator input schema")
      input_schema = json.load(open(input_schema_path, "r"), object_pairs_hook=collections.OrderedDict)
      record = SimulatorInputSchema.query().insert(dict(DocumentStore.ordered_hash(input_schema)))
      simulator.input_schema_document_id = str(record)
    else:
      print("Warning: Could not find %s" % (input_schema_path))

    # Look for output schema, etc
    output_schema_path = "%s/occam/output_schema.json" % (simulator.local_path)
    if os.path.exists(output_schema_path):
      print("Commiting simulator output schema")
      output_schema = json.load(open(output_schema_path, "r"), object_pairs_hook=collections.OrderedDict)
      record = SimulatorOutputSchema.query().insert(dict(DocumentStore.ordered_hash(output_schema)))
      simulator.output_schema_document_id = str(record)
    else:
      print("Warning: Could not find %s" % (output_schema_path))

    # Look for recipes
    print("Raking recipes")
    recipe_path = "%s/occam/recipes" % (simulator.local_path)
    if os.path.exists(recipe_path):
      print("Found recipes in %s" % (recipe_path))
      # Look for every json file in the recipes path
      for filename in os.listdir(recipe_path):
        if filename[-5:] == '.json':
          print("Adding recipe found at %s/%s" % (recipe_path, filename))
          # Read json description
          recipe_json = json.load(open("%s/%s" % (recipe_path, filename), "r"))

          # Create a recipe for each one
          recipe = Recipe()
          if "name" in recipe_json:
            recipe.name            = recipe_json["name"]
            recipe.simulator_id    = simulator.id
            if "description" in recipe_json:
              recipe.description     = recipe_json["description"]

            record = Configuration.query().insert(DocumentStore.ordered_hash(recipe_json["configuration"]))
            recipe.configuration_document_id = str(record)

          DB.session().add(recipe)
