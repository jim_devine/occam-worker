import os
import json
import threading
import time
from datetime import date

from models.job import Job
from models.system import System

from time import sleep

from lib.document_store import DocumentStore
from lib.db             import DB
from lib.builder        import Builder
from lib.installer      import Installer
from lib.runner         import Runner
from lib.daemon         import Daemon
from lib.signal         import Signal
from lib.memcache       import Memcache

class Worker:
  def __init__(self, options):
    self.options = options

    DB.initialize(self.options)

    self.system = DB.session().query(System).first()

    if self.system is None:
      # Panic
      print("OCCAM System not initialized. Run configuration in a web browser first.")
      exit(-1)
      return

    if self.options.rootPath is None:
      self.options.rootPath = self.system.jobs_path

    self.options.simulatorPath = self.system.simulators_path

    if not os.path.exists(self.options.rootPath):
      print("Making jobs directory: %s" % (self.options.rootPath))
      os.mkdir(self.options.rootPath)

    self.workersPath = "%s/workers" % (self.options.rootPath)

    if not os.path.exists(self.workersPath):
      print("Making workers directory: %s" % (self.workersPath))
      os.mkdir(self.workersPath)

    if self.options.daemon:
      DB.close()
      retCode = Daemon(self.options.rootPath, "%s/occam-worker" % (self.workersPath), True).create()
      # Reopen the database in the daemon process
      DB.initialize(self.options)

    # Open document store
    DocumentStore.initialize(self.options)

    # Open memcache access
    Memcache.initialize(self.options)

    self.lock = threading.Lock()

    self.signaller = Signal({
      'finished': self.signal,
      'worker':   self,
    })

    retCode = 0

    procParams = """
    {
      "exit_code":          "%s",
      "process_id":         "%s",
      "parent_process_id":  "%s",
      "parent_group_id":    "%s",
      "session_id":         "%s",
      "user_id":            "%s",
      "effective_user_id":  "%s",
      "real_group_id":      "%s",
      "effective_group_id": "%s"
    }
    """ % (retCode, os.getpid(), os.getppid(), os.getpgrp(), os.getsid(0),
                    os.getuid(), os.geteuid(), os.getgid(),  os.getegid())

    open("%s/occam-worker-%s.json" % (self.workersPath, os.getpid()), "w").write(procParams + "\n")

  def createJobPath(self, job):
    now = date.today()

    year_path  = "%s/%s" % (self.options.rootPath, now.year)
    month_path = "%s/%s" % (year_path,             now.month)
    day_path   = "%s/%s" % (month_path,            now.day)
    job_path   = "%s/job-%s" % (day_path,              job.id)

    if not os.path.exists(year_path):
      print("Making year directory: %s" % (year_path))
      os.mkdir(year_path)

    if not os.path.exists(month_path):
      print("Making month directory: %s" % (month_path))
      os.mkdir(month_path)

    if not os.path.exists(day_path):
      print("Making day directory: %s" % (day_path))
      os.mkdir(day_path)

    if not os.path.exists(job_path):
      print("Making job directory: %s" % (job_path))
      os.mkdir(job_path)

    return job_path

  def run(self):
    while True:
      if not self.idle():
        if self.options.verbose:
          print("No work")
        sleep(3)

  # Called when the worker is idle
  def idle(self):
    # Yank all jobs from the database
    for job in DB.session().query(Job).filter_by(status="queued",depends_on_id=None):
      # Perform a job
      self.perform(job)
      return True

    return False

  # Do the given job
  def perform(self, job):
    # Remember the current job
    self.job = job

    if self.options.verbose:
      print("Started job #" + str(self.job.id))

    # Update job status to running
    self.job.status = "running"
    DB.session().commit()

    self.lock.acquire()

    # Establish start time
    start_time = time.time()

    # Create a directory for the job to run in with the format:
    # {occam-root}/%yyyy/%mm/%dd/job-{job id}
    jobWorkingPath = self.createJobPath(self.job)

    # Afterward, this directory will have a 'job-{job id}.json' and a
    # 'job-{job id}.log' which details its running environment and its
    # output respectively.

    # Determine who acts for this job
    if self.job.simulator_id != None:
      if self.job.kind == "install":
        print("Simulator install job")
        self.task = Installer(self.options, jobWorkingPath)
      elif job.kind == "build":
        print("Simulator build job")
        self.task = Builder(self.options, jobWorkingPath)
    else:
      print("Experiment job")
      self.task = Runner(self.options, jobWorkingPath)

    # Run the task for the job
    if not self.task.run(self.job):
      self.fail()

    # Wait for signal meaning the job is done
    print("Waiting")

    def tail(f, window=20):
      """
      Returns the last `window` lines of file `f` as a list.
      """
      if window == 0:
        return []
      BUFSIZ = 1024
      f.seek(0, 2)
      bytes = f.tell()
      size = window + 1
      block = -1
      data = []
      while size > 0 and bytes > 0:
        if bytes - BUFSIZ > 0:
          # Seek back one whole BUFSIZ
          f.seek(block * BUFSIZ, 2)
          # read BUFFER
          data.insert(0, f.read(BUFSIZ).decode("utf-8"))
        else:
          # file too small, start from begining
          f.seek(0,0)
          # only read what was not read
          data.insert(0, f.read(bytes).decode("utf-8"))

        linesFound = data[0].count('\n')
        size -= linesFound
        bytes -= BUFSIZ
        block -= 1
      return ''.join(data)

    i = 0
    jobLogFilePath = "%s/job-%s.log" % (jobWorkingPath, self.job.id)
    outputLogFilePath = "%s/output.raw" % (jobWorkingPath)
    while self.lock.locked():
      # Monitoring/output pushing
      stats = {"foo": i}
      i = i + 1
      # Tail the job output to memcache
      if os.path.exists(jobLogFilePath):
        jobFile = open(jobLogFilePath, "rb")
        stats["job_log"] = tail(jobFile)
        jobFile.close()
      if os.path.exists(outputLogFilePath):
        jobFile = open(outputLogFilePath, "rb")
        stats["output_log"] = tail(jobFile)
        jobFile.close()

      Memcache.set("job_%s" % (self.job.id), stats)
      sleep(0.5)

    if os.path.exists(jobLogFilePath):
      jobFile = open(jobLogFilePath, "rb")
      stats["job_log"] = tail(jobFile)
      jobFile.close()
    if os.path.exists(outputLogFilePath):
      jobFile = open(outputLogFilePath, "rb")
      stats["output_log"] = tail(jobFile)
      jobFile.close()

    Memcache.set("job_%s" % (self.job.id), stats)

    # Record time
    end_time = time.time()

    elapsed_time = end_time - start_time
    print("Elapsed Time: %s seconds" % (elapsed_time))

    self.job.elapsed_time = elapsed_time

    # Commit the job updates
    DB.session().commit()

    # Perform other scripts
    self.task.finish(self.job, jobWorkingPath)

    DB.session().commit()

  def fail(self):
    if self.options.verbose:
      print("Failed job #" + str(self.job.id))

    # Update job to failed
    self.job.status = "failed"

    # Release poll lock to poll once more
    self.lock.release()

  def signal(self):
    """This will be called when the dispatched process wishes to communicate.
    """

    # Retrieve the child pid
    try:
      spawned = json.load(open("%s/current.json" % (self.options.rootPath)))

      # Log
      print("Message from child [%s]" % (spawned["process_id"]))
    except:
      print("Problem reading %s/current.json." % (self.options.rootPath))

    # Remove log
    try:
      os.remove("%s/current.json" % (self.options.rootPath))
    except:
      print("Problem deleting %s/current.json." % (self.options.rootPath))

    self.job.log_file = "%s/job-%s.json" % (self.options.rootPath, self.job.id)

    # Mark job as finished
    self.finish()

  def finish(self):
    if self.options.verbose:
      print("Finished job #" + str(self.job.id))

    # Update dependent jobs (if any)
    for dependent in DB.session().query(Job).filter_by(status="queued",depends_on_id=self.job.id):
      dependent.depends_on_id=None

    # Update job status to finished
    self.job.status = "finished"

    # Release poll lock to start polling for jobs once more
    self.lock.release()
