#!/usr/bin/env python

import subprocess
import os
import sys
import time
import datetime


class Monitor:
	
	def _init_(self, pid):
		self.pid = pid
		self.count = 0
		self.dict = {}
		self.vmpeak_max = 0
		self.vmpeak_average = 0
		self.vmsize_max = 0
		self.vmsize_average = 0
		self.vmhwm_max = 0
		self.vmhwm_average = 0
		self.vmrss_max = 0
		self.vmrss_average = 0
		self.vmdata_max = 0
		self.vmdata_average = 0
				
	
	def status(self):
		
		self.count = self.count + 1

		command = "/proc/" + str(self.pid) + "/status"
		
		f = open(command, "r")
		dict = {}

		while True:
			string = f.readline()
			#checks for end of file			
			if len(string) == 0:
				break
			
			array = string.split(":")
			dict[array[0]] = array[1].strip()
		


		self.dict.update(dict)
		self.vmpeak()
		self.vmsize()
		self.vmhwm()
		self.vmrss()
		self.vmdata()
		return dict

		
	def vmpeak(self):

		
		temp_vmpeak = self.parse('VmPeak')

		if temp_vmpeak > self.vmpeak_max:
			self.vmpeak_max = temp_vmpeak

		self.vmpeak_average = self.vmpeak_average + temp_vmpeak	

	def max_vmpeak(self):

		return self.vmpeak_max

	def average_vmpeak(self):
		
		return self.vmpeak_average/self.count

	def vmsize(self):
		
		temp_vmsize = self.parse('VmSize')
	
		if temp_vmsize > self.vmsize_max:
			self.vmsize_max = temp_vmsize

		self.vmsize_average = self.vmsize_average + temp_vmsize

	def max_vmsize(self):

		return self.vmsize_max

	def average_vmsize(self):
		
		return self.vmsize_average/self.count

	def vmhwm(self):
		
		temp_vmhwm = self.parse('VmHWM')
			
		if temp_vmhwm > self.vmhwm_max:
			self.vmhwm_max = temp_vmhwm

		self.vmhwm_average = self.vmhwm_average + temp_vmhwm

	def max_vmhwm(self):

		return self.vmhwm_max

	def average_vmhwm(self):
		
		return self.vmhwm_average/self.count

	def vmrss(self):
		
		temp_vmrss = self.parse('VmRSS')
		

		if temp_vmrss > self.vmrss_max:
			self.vmrss_max = temp_vmrss

		self.vmrss_average = self.vmrss_average + temp_vmrss

	def max_vmrss(self):

		return self.vmrss_max

	def average_vmrss(self):
		
		return self.vmrss_average/self.count

	def vmdata(self):
		
		temp_vmdata = self.parse('VmData')


		if temp_vmdata > self.vmdata_max:
			self.vmdata_max = temp_vmdata

		self.vmdata_average = self.vmdata_average + temp_vmdata

	def max_vmdata(self):

		return self.vmdata_max

	def average_vmdata(self):
		
		return self.vmdata_average/self.count

	def parse(self, key):

		array = self.dict[key].strip().split(" ")
		return int(array[0])

	

		
			
				

			