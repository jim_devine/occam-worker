import signal

_signaller = None

def finish(signum, frame):
  _signaller.options['finished']()
  return

class Signal:
  def __init__(self, options):
    self.options = options

    global _signaller
    _signaller = self

    signal.signal(signal.SIGUSR1, finish)
    return

  def wait(self):
    signal.pause()
